import { EntFetcherService, IAnyApiService } from "src/app/types/app.types";
import { EntAny } from "src/app/types/ent.types";
import { Subject, Subscription, filter, map, take } from "rxjs";
import { NgZone } from "@angular/core";

const _singletons:{ [code:string]: SegmentedList } = {};

const MIN_PORTION_SIZE = 3;
const MAX_SEGMENT_SIZE = 30;

export class SegmentedList<T = EntAny>{

    static getSingleton( api:IAnyApiService, ngz:NgZone, key:string, baseRequestParams:any ){
        const code = `${key}#${baseRequestParams ? JSON.stringify(baseRequestParams) : 'all'}`;
        return _singletons[code]
            ? _singletons[code]
            : _singletons[code] = new SegmentedList(api, ngz, key, baseRequestParams);
    }
    
    isDefinite = false;
    isExhausted = false;

    did$: Subject<string> = new Subject();

    items: T[] = [];
    pageItems!: T[];
    pages: string[] = [];
    total!: number;
    totalPages: number = 0;
    currentPage!: string;
    
    #pageSize = 0;
    #searchQuery: string | null = null;
    #didS!: Subscription;
    #progressS!: Subscription;

    constructor(
        private api: IAnyApiService,
        private ngz: NgZone,
        private key: string,
        private baseRequestParams: any,
    ){
        this.did$.pipe(
            filter( code => code === 'retrieved' ),
        ).subscribe(()=>{
            this.#redefinePages();
            if( !this.#isPageSizeDefined ) this.#did('portion');
            else if( !this.isExhausted ) this.topUp();
        });
    }

    //#region _I
    get isEmpty(): boolean { return !this.items.length; }
    get isInProgress(): boolean { return this.#progressS && !this.#progressS.closed; }
    get needPageSize(): boolean { return !this.#isPageSizeDefined && !this.isInProgress; }
    
    retrievePortion(){
        if( this.isExhausted ) return this.#did('retrieved');

        const portionSize = this.#isPageSizeDefined ? this.#pageSize : MIN_PORTION_SIZE;
        const itMakesSenseToContinue = 0 === this.items.length % portionSize;
        if( !itMakesSenseToContinue ) return this.#exhausted();

        const fetchingMethod = this.api.getFetchingMethod(this.key);
        if( !fetchingMethod ) return;

        const lastPage = this.items.length / portionSize;
        this.#progressS = (this.api as unknown as EntFetcherService)[fetchingMethod]({
            ...this.baseRequestParams,
            pageNum: lastPage + 1,
            pageSize: portionSize,
            searchQuery: this.#searchQuery,
        }).pipe(
            map( portion =>{
                if( !portion || !portion.length )
                    return this.#exhausted();
                
                for( let idx=0; idx < portion.length; idx++ )
                    if( portion[idx] )
                        this.items.push(portion[idx]);
                
                this.#did('retrieved');
            })
        ).subscribe();
    }
    
    definePageSize( pageSize:number = this.items.length ){
        if( pageSize > 0 ){
            this.#pageSize = Math.min(pageSize, MAX_SEGMENT_SIZE);
            this.#did('definited');
        }
    }

    topUp(){
        if( !this.isExhausted )
            this.retrievePortion();
    }

    refresh( searchQuery?:string ){
        this.#did('toRefresh');
        this.items.splice(0, this.items.length);
        this.isExhausted = false;
        this.#searchQuery = searchQuery || null;
        this.#redefinePages();
        this.topUp();
    }

    toPage( page:any ){
        this.#redefinePages(Number.isInteger(+page) ? +page : 0);
    }

    reset(){
        this.#pageSize = 0;
        this.refresh();
        this.#did('undefinite');
    }
    
    dismiss(){
        this.#didS?.unsubscribe()
        this.#progressS?.unsubscribe();
    }
    //#endregion

    //#region __
    get #isPageSizeDefined(){ return this.#pageSize > 0; }

    #did( code:string ){
        this.ngz.onStable.pipe( take(1) ).subscribe(
            ()=> this.did$.next(code)
        )
    }

    #exhausted(){
        this.isExhausted = true;
        if( !this.#isPageSizeDefined ) this.definePageSize(this.items.length);
        this.#did('retrieved');
        this.#did('exhausted');
    }

    #redefinePages( page:number = 1 ){
        this.ngz.onStable.pipe( take(1) ).subscribe(()=>{

            this.pages = [];
            this.currentPage = ''+page;
            this.total = this.items.length;

            if( this.#isPageSizeDefined ){
                this.totalPages = Math.ceil(this.total / this.#pageSize);
                const minPage = Math.max(2, page - 1);
                const maxPage = Math.min(this.totalPages - 1, page + 1);
                if( this.totalPages > 1 ){
                    this.pages.push('1');

                    if( minPage > 3 )
                        this.pages.push(`-${ minPage - 2 }`);

                    else if( minPage > 2 )
                        this.pages.push('2');

                    for( let i = minPage; i <= maxPage; i++ )
                        this.pages.push(`${i}`);

                    if( maxPage < this.totalPages - 2 )
                        this.pages.push(`-${ maxPage + 2 }`);

                    else if( maxPage === this.totalPages - 2 )
                        this.pages.push(`${ this.totalPages - 1 }`);
                    
                    this.pages.push(`${this.totalPages}`);
                }
                this.pageItems = this.items.slice(
                    this.#pageSize * (page - 1),
                    this.#pageSize * page
                );
                this.isDefinite = true;

            }else{
                this.isDefinite = false;
                this.pageItems = this.items;
                this.totalPages = 1;
            }
        })
    }
    //#endregion
}
