import { Routes } from '@angular/router';
import { CatPage } from './pages/cat/cat.page';
import { LoginPage } from './pages/login/login.page';

export const routes: Routes = [
    {
        path: 'login', component: LoginPage
    },
    {
        path: 'pets',
        loadComponent: () => import('src/app/pages/pets/pets.page').then((m) => m.PetsPage),
    },
    {
        path: 'cat/:cat_id', component: CatPage
    },
    
    { path: '', pathMatch: 'full', redirectTo: 'pets' },
    { path: '**', redirectTo: 'login' },
];
