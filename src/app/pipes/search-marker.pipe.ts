import { Pipe, PipeTransform } from "@angular/core";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";

@Pipe({
    standalone: true,
    name: "searchMarker"
})
export class SearchMarkerPipe implements PipeTransform {
    constructor( 
        private sanitized: DomSanitizer
    ){}
        
    transform(
        src: any,
        token: string | undefined,
        type: 'frags' | 'full' = 'full', except?:string[]
    ): SafeHtml|null {
        if( typeof src === 'object' ){
            src = {...src};
            if( except )for( let key of except ) delete src[key];
            src = Object.values(src).join('; ');
        }else
            src = ''+src;

        let html: string | undefined;
        if( type === 'full' ){
            if( !token ) return src;
            html = src.replace(
                new RegExp(`(${token})`, 'gi'),
                `<ion-text color="tertiary" class="paw-search-token">$1</ion-text>`
            );
        }else{
            html = '';
            const re = new RegExp(`(.{0,7})(${token})(.{0,7})`, 'gi');
            let cnt = 0;
            let tokens;
            while( null !== (tokens = re.exec(src)) ){
                html += `...${tokens[1]}<ion-text color="tertiary" class="paw-search-token">${tokens[2]}</ion-text>${tokens[3]}...<br/>`;
                if( ++cnt > 4 ) break;
            }
        }
        return html ? this.sanitized.bypassSecurityTrustHtml(html) : null;
    }
}
