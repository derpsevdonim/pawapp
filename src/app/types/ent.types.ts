export type EntCore = {
    id: string,
}

export type EntAny = EntCore & any;

export type CatType = EntCore & {
    name: string,
    image: string,
    breed: string,
    age: string,
    description: string,
    contact_name: string,
    contact_phone: string,
    is_adopted: boolean,

    /** Cat's record creaton timestamp */
    listed_at: string,

    /** A premium cat property */
    adoption_fee?: string
}
