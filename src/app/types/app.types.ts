import { Observable } from "rxjs";

export interface IAnyApiService {
    getFetchingMethod: ( key:string )=> string | undefined;
}

export type EntFetcherService<T = any> ={
    [key:string]: ( options?:FetchingOptions )=> Observable<T[]>;
}

export type FetchingOptions ={
    searchQuery?: string,
    pageNum?: number,
    pageSize?: number,
}

export type RequestParams_common ={
    search?: string,
    page?: number,
    p?: number,
    limit?: number,
    l?: number,
}
