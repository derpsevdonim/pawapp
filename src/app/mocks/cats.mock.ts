export const CATS = [
  {
    "listed_at": "2024-04-06T00:38:02.451Z",
    "name": "Bailey",
    "image": "https://loremflickr.com/640/480/cats",
    "age": "4",
    "breed": "Savannah",
    "description": "Delectus praesentium aperiam adipisci culpa tenetur facilis voluptatem nemo ut. Commodi quisquam repudiandae voluptatem quasi nam dolorum. At corporis quia magni blanditiis pariatur dolorem. Iure quas officia impedit laborum. Molestiae qui vero.",
    "contact_name": "Abel Borer",
    "contact_phone": "539-908-9870 x3492",
    "id": "1"
  },
  {
    "listed_at": "2024-04-05T10:44:46.559Z",
    "name": "Arden",
    "image": "https://loremflickr.com/640/480/cats",
    "age": "4",
    "breed": "Tonkinese",
    "description": "Eos architecto qui ut quaerat delectus et. Accusamus mollitia praesentium. Esse deserunt dolorem voluptatem qui porro. Pariatur reiciendis incidunt dicta similique tenetur voluptatibus consequuntur. Odio consequatur sit deleniti quo esse.",
    "contact_name": "Elsa Bogan",
    "contact_phone": "512-664-0621 x597",
    "id": "2"
  },
  {
    "listed_at": "2024-04-05T08:36:37.483Z",
    "name": "Kyle",
    "image": "https://loremflickr.com/640/480/cats",
    "age": "4",
    "breed": "Munchkin",
    "description": "Delectus sed hic nihil amet illo necessitatibus quasi deleniti. Mollitia eius cumque quidem iste voluptatem iste voluptate vitae. Dolorem voluptatem quo excepturi architecto autem.",
    "contact_name": "Shaun Hintz",
    "contact_phone": "982.479.2254 x19471",
    "id": "3"
  },
]
