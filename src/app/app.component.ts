import { Component } from '@angular/core';
import { IonApp, IonRouterOutlet } from '@ionic/angular/standalone';

import { addIcons } from 'ionicons';
import * as ICONS_ALL from 'ionicons/icons';

import { AppService } from './services/app.service';

const LOG_KEY_BASEFONT = 'font-family: monospace;';
const LOG_VAL_BASEFONT = 'font-family: monospace; font-size: 1.4em;';
const LOG_DOT_STYLES = ['color: #888', ''];
const ICONS_CUSTOM = {
    'paw-logo': 'assets/logo_paws.svg'
}

declare global {
    interface Window {
        $app: AppService;
        readonly $env: string;
    }
}

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    standalone: true,
    imports: [IonApp, IonRouterOutlet],
})
export class AppComponent {
    constructor() {
        addIcons({...ICONS_ALL, ...ICONS_CUSTOM});
        Object.defineProperty(
            window, "$env", { get(){
                const filterFn =( k:string )=> k[0] !== '_' && typeof this.$app[k] !== 'function';
                const leftWidth = Object.getOwnPropertyNames(this.$app)
                    .filter( filterFn )
                    .reduce(( maxLength, key )=> Math.max(key.length, maxLength), 0);
                    
                console.groupCollapsed
                ? console.groupCollapsed('[ENV][APP]')
                : console.log('[ENV][APP]:');
                
                Object.getOwnPropertyNames(this.$app)
                    .filter( filterFn )
                    .forEach( key =>{
                        const color = (()=>{
                            switch( typeof this.$app[key] ){
                                case 'number': return '#4AE900';
                                case 'string': return '#EDC700';
                                case 'object': return this.$app[key] ? '#8BA455' : '#9A9A9A';
                                case 'boolean': return this.$app[key] ? '#DE80FF' : '#6450E3';
                            }
                            return 'initial';
                        })();
                        const keySide = `%c${(key + '%c').padEnd(leftWidth + 4, '.')}%c`;
                        const valSide = ''+this.$app[key];
                        if( /https?:/.test(valSide) ) console.log(
                            `${keySide} ${valSide}`,
                            LOG_KEY_BASEFONT + ' background: rgba(100 100 255 / 0.3)', ...LOG_DOT_STYLES
                        );
                        else console.log(
                            `${keySide} ${('%c'+valSide+'%c').padEnd(70)}`,
                            LOG_KEY_BASEFONT, ...LOG_DOT_STYLES,
                            `${LOG_VAL_BASEFONT} background: rgb(40, 40, 40); border-radius: 1px; padding: 4px; margin: -4px; color: ${color};`,
                            `${LOG_VAL_BASEFONT} background: rgba(40 40 40 / 0.9); padding: 4px 0; margin: -4px 0;`
                        );
                    });

                if( !!console.groupCollapsed ) console.groupEnd();
                return new Date().toISOString();
            }}
        );
    }
}
