import { Component } from '@angular/core';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { CurrencyPipe } from '@angular/common';
import { JsonPipe, NgFor, NgIf } from '@angular/common';
import { IonAvatar, IonBackButton, IonBadge, IonButton, IonButtons, IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonContent, IonHeader, IonIcon, IonItem, IonLabel, IonList, IonListHeader, IonModal, IonText, IonThumbnail, IonTitle, IonToolbar } from '@ionic/angular/standalone';
import { NavController } from '@ionic/angular/standalone';
import { Subscription } from 'rxjs';

import { AmService } from 'src/app/services/am.service';
import { AppService } from 'src/app/services/app.service';
import { ApiMockIoService } from 'src/app/services/api_mockio.service';
import { CatType } from 'src/app/types/ent.types';

@Component({
    standalone: true,
    selector: 'app-cat-page',
    templateUrl: './cat.page.html',
    styles:[`
        :host > ion-content {
            background: var(--ion-color-medium-tint);
            --background: url(/assets/bg_paws.svg) center center repeat;
            &._premium {
                background: var(--ion-color-secondary-back);
            }
            ion-card{
                animation: fadeIn 2s var(--bezier);
            }
        }
    `],
    imports: [
        NgIf, NgFor, JsonPipe, CurrencyPipe,

        IonAvatar,
        IonBackButton,
        IonBadge,
        IonButton, IonButtons,
        IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle,
        IonContent,
        IonHeader,
        IonIcon,
        IonItem,
        IonLabel,
        IonList, IonListHeader,
        IonModal,
        IonText,
        IonThumbnail,
        IonTitle,
        IonToolbar,
        
        RouterLink,
    ],
})
export class CatPage {
    catId!: string;

    cat!: CatType | undefined;
    catS!: Subscription;
    constructor(
        public am: AmService,
        public app: AppService,
        private api_mio: ApiMockIoService,
        private route: ActivatedRoute,
        private navCtrl: NavController
    ){}

    ngOnInit(): void {
        this.catId = this.route.snapshot.params['cat_id'];
        if( this.catId )
            this.catS = this.api_mio
                .fetchCat$(this.catId)
                .subscribe( cat =>{
                    if( cat )
                        this.cat = cat;
                    else
                        this.navCtrl.back();
                });
        else this.navCtrl.back();
    }

    ngOnDestroy(): void {
        if( this.catS ) this.catS.unsubscribe();
    }
}
