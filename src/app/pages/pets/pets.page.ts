import { Component, ElementRef, NgZone, ViewChild } from '@angular/core';
import { CurrencyPipe, JsonPipe, NgFor, NgIf } from '@angular/common';
import { RouterLink } from '@angular/router';
import { IonSearchbarCustomEvent, SearchbarInputEventDetail, IonRefresherCustomEvent, RefresherEventDetail } from '@ionic/core';
import { IonHeader, IonToolbar, IonTitle, IonContent, IonAvatar, IonList, IonItem, IonThumbnail, IonLabel, IonButtons, IonIcon, IonButton, IonPopover, IonText, IonBadge, IonSearchbar, IonFooter, IonSegment, IonSegmentButton, IonRefresherContent, IonRefresher, IonProgressBar } from '@ionic/angular/standalone';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Subject, Subscription, tap, throttleTime } from 'rxjs';

import { AmService } from 'src/app/services/am.service';
import { AppService } from 'src/app/services/app.service';
import { CfgService } from 'src/app/services/cfg.service';
import { AsyncService } from 'src/app/services/async.service';
import { ApiMockIoService } from 'src/app/services/api_mockio.service';
import { SearchMarkerPipe } from 'src/app/pipes/search-marker.pipe';
import { SegmentedList } from 'src/app/classes/segmented-list.class';
import { CatType } from 'src/app/types/ent.types';

@Component({
    selector: 'app-pets-page',
    templateUrl: 'pets.page.html',
    styleUrls: ['pets.page.scss'],
    standalone: true,
    imports: [
        NgFor, NgIf, CurrencyPipe, JsonPipe,
    
        IonAvatar,
        IonBadge,
        IonButton, IonButtons,
        IonContent,
        IonFooter,
        IonHeader,
        IonIcon,
        IonItem,
        IonLabel,
        IonList,
        IonPopover,
        IonProgressBar,
        IonRefresher, IonRefresherContent,
        IonSearchbar,
        IonSegment, IonSegmentButton,
        IonText,
        IonThumbnail,
        IonTitle,
        IonToolbar,
        
        RouterLink,

        SearchMarkerPipe,
    ],
    animations: [
        trigger('inOutAnimation',[
            state('*', style({ opacity: 0.1 })),
            transition(':enter',[
                style({ opacity: 0 }),
                animate('.5s ease-out', style({ opacity: 0.1 }))
            ]),
            transition(':leave',[
                style({ opacity: 0.1 }),
                animate('.5s ease-in', style({ opacity: 0 }))
            ])
        ]),
        trigger('inAnimation',[
            state('*', style({ opacity: 1 })),
            transition(':enter',[
                style({ opacity: 0 }),
                animate('.3s ease-out', style({ opacity: 1 }))
            ]),
        ])
    ]
})
export class PetsPage {
    @ViewChild(IonSearchbar, {read: IonSearchbar}) private searchBar!: IonSearchbar;
    @ViewChild(IonContent, {read: ElementRef}) private contentEl!: ElementRef;
    @ViewChild(IonList, {read: ElementRef}) private listEl!: ElementRef;
    @ViewChild(IonSegment) segment!: IonSegment;

    catsList: SegmentedList<CatType> = SegmentedList.getSingleton(this.api_mio, this.ngz, 'cats', {});;

    lastSearchQuery: string | undefined;
    isInitialising = false;
    isListRefreshing = false;
    isPressingList = false;
    
    #isHeightDetectionActive = true;
    #viewCheck$ = new Subject<void>();
    #viewCheckS!: Subscription;
    #listS!: Subscription;
    #asyncS!: Subscription;
    
    constructor(
        public am: AmService,
        public app: AppService,
        private cfg: CfgService,
        private async: AsyncService,
        private api_mio: ApiMockIoService,
        private ngz: NgZone,
    ){}

    //#region __Lifecycle
    ngOnInit(): void {
        this.#asyncS = this.async
            .triggerOn$('env_auth')
            .subscribe(()=>{ this.#resetState(); });
    }

    ngAfterViewInit(){
        this.#viewCheckS = this.#viewCheck$.pipe(
            throttleTime(100),
            tap(()=>{
                this.#doHeightDetection();
                this.#syncPageNum();
            })
        ).subscribe();
        this.#listS = this.catsList.did$.subscribe( code =>{
            if(~[ 'portion', 'undefinite' ].indexOf(code)){
                this.#definePageSize();
            }else switch( code ){
                case 'toRefresh': this.isListRefreshing = true; break;
                case 'retrieved': this.isListRefreshing = false; break;
                case 'definited': this.#topUpList(); break;
                case 'exhausted': this.#unsyncPageNum(); break;
            }
        });
        this.catsList.topUp();
    }

    ngAfterViewChecked(){
        this.#viewCheck$.next();
    }

    ngOnDestroy(): void {
        this.catsList.dismiss();
        this.#listS?.unsubscribe();
        this.#asyncS?.unsubscribe();
        this.#viewCheckS?.unsubscribe();
    }
    //#endregion

    //#region __I
    doRefresh( ev:IonRefresherCustomEvent<RefresherEventDetail> ){
        this.catsList.reset();
        setTimeout(()=> ev.target.complete(), 500);
    }

    handleInput( event:IonSearchbarCustomEvent<SearchbarInputEventDetail> ){
        const query = event.target.value?.toLowerCase();
        if( this.lastSearchQuery === query ) return;
        this.lastSearchQuery = query;
        this.#refreshList();
        this.#unsyncPageNum();
    }

    gotoPage( page:string ){
        if( page[0] === '-' ){
            this.#unsyncPageNum();
            this.catsList.toPage(page.slice(1));
        }else
            this.catsList.toPage(page);
    }

    async logout(){
        await this.cfg.setAndStore('userAuth', null);
        this.async.did('env_auth');
    }
    //#endregion

    //#region __
    #resetState(){
        this.searchBar.value = '';
        this.lastSearchQuery = '';
        this.catsList.reset();
    }

    #definePageSize(){
        this.#isHeightDetectionActive = true;
        this.isInitialising = true;
        this.#unsyncPageNum();
        this.#viewCheck$.next();
    }

    #topUpList(){
        this.catsList.topUp();
        this.isInitialising = false;
    }

    #unsyncPageNum(){
        this.segment.value = undefined;
    }

    #syncPageNum(){
        if( this.segment.value !== this.catsList.currentPage )
            this.segment.value = this.catsList.currentPage;
    }

    #refreshList(){
        this.catsList.refresh(this.lastSearchQuery);
    }

    #doHeightDetection(){
        if( this.#isHeightDetectionActive && this.catsList.needPageSize ){
            this.#isHeightDetectionActive = false;
            const listH = this.listEl.nativeElement.scrollHeight;
            const viewH = this.contentEl.nativeElement.scrollHeight - 62;
            if( listH < viewH )
                this.catsList.retrievePortion();
            else
                this.catsList.definePageSize();
        }
    }
    //#endregion
}
