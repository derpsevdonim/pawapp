import { Component } from '@angular/core';
import { IonContent, IonList, IonInput, IonItem, IonListHeader, IonLabel, IonIcon, IonButton, NavController } from '@ionic/angular/standalone';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { AsyncService } from 'src/app/services/async.service';
import { CfgService } from 'src/app/services/cfg.service';

@Component({
    standalone: true,
    selector: 'app-login-page',
    templateUrl: './login.page.html',
    styleUrl: './login.page.scss',
    imports: [
        IonButton, IonIcon, IonLabel, IonListHeader,
        IonItem, IonInput, IonList, IonContent,
        ReactiveFormsModule, FormsModule
    ],
})
export class LoginPage {
    
    credentialsFg!: FormGroup;
    
    constructor(
        private cfg: CfgService,
        private async: AsyncService,
        private navCo: NavController,
        private fb: FormBuilder,
    ){}

    ngOnInit(): void {
        this.credentialsFg = this.fb.group({
            email: ['', [Validators.required, Validators.email]],
            pass: ['', [Validators.required]]
        });
    }

    ionViewWillLeave(){
        this.credentialsFg.reset();
    }

    async login(){
        await this.cfg.setAndStore('userAuth', this.credentialsFg.value);
        this.async.did('env_auth');
        this.navCo.navigateForward('/pets');
    }

    async justGo(){
        await this.cfg.setAndStore('userAuth', null);
        this.async.did('env_auth');
        this.navCo.navigateForward('/pets');
    }
}
