import { Injectable } from "@angular/core";
import { AppService } from "./app.service";
import { StorageService } from "./storage.service";

@Injectable({ providedIn: 'root' })
export class CfgService{
    constructor(
        private app: AppService,
        private store: StorageService,
    ){}

    async restoreStates(){
        this.app.patch('userAuth', await this.store.getAsObject('userAuth'));
    }

    async setAndStore( key:string, value:string|Object|null ){
        this.app.patch(key, value);

        value
        ? typeof value === 'string'
            ? await this.store.setAsString(key, ''+value)
            : await this.store.setAsObject(key, value)
        : await this.store.delAny(key);
    }
}
