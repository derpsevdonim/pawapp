import { Injectable } from "@angular/core";
import { ToastController } from '@ionic/angular/standalone';

@Injectable({ providedIn: 'root' })
export class AmService{
    #lastErrorMessage!: string | null;

    constructor( private toast: ToastController ){}

    imgErrorHandle( ev: ErrorEvent, holderImgSrc = '/assets/noimg_cat.svg' ){
        const img = ev.target as HTMLImageElement;
        img.src = holderImgSrc;
    }

    async noteError( message: string, duration = 10000 ){
        if( !message || this.#lastErrorMessage === message )
            return console.warn('[noty][empty_or_double]:', message);

        this.#lastErrorMessage = message;
        const toast = await this.toast.create({
            message: message || '...',
            color: 'danger', position: 'top',
            buttons: [{ text: 'OK', role: 'cancel' }],
            cssClass: 'paw-toast',
            duration,
        });
        await toast.present();
        await toast.onDidDismiss();
        if( this.#lastErrorMessage === message ) this.#lastErrorMessage = null;
    }
}
