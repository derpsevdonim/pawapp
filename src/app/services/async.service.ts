import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { filter, debounceTime } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AsyncService{

    #did$:Subject<string> = new Subject();
    #triggers$$:{ [key:string]: Observable<any> } = {};

    //#region __I
    triggerOn$(
        keysOption: Set<string> | string[] | string | RegExp,
        bounceTime: number = 200
    ): Observable<string> {
        const triggerKey =
            keysOption instanceof RegExp
            ? this.#provideTrigger_regexp( keysOption, bounceTime )
            : this.#provideTrigger_keys( keysOption, bounceTime );

        return this.#triggers$$[triggerKey];
    }
    
    did( actionKey: string | string[] ){
        if( actionKey instanceof Array )
            [...actionKey].forEach( key => this.#did$.next(key));
        else
            this.#did$.next(actionKey);
    }
    //#endregion

    //#region __
    #provideTrigger_regexp( re: RegExp, bounceTime: number ): string {
        const triggerKey = re.source + bounceTime;
        if( !this.#triggers$$[triggerKey] )
            this.#triggers$$[triggerKey] = this.#did$.pipe(
                filter( key => re.test(key) ),
                debounceTime(bounceTime),
            );
        return triggerKey;
    }

    #provideTrigger_keys( keys: Set<string> | string[] | string, bounceTime: number ): string {
        const triggerKey = Array.from(keys).join() + bounceTime;
        if( !this.#triggers$$[triggerKey] ){
            const keysSet:Set<string> =
                keys instanceof Set
                ? keys
                : keys instanceof Array
                    ? new Set(keys)
                    : new Set([keys]);

            this.#triggers$$[triggerKey] = this.#did$.pipe(
                filter( key => keysSet.has(key) ),
                debounceTime(bounceTime),
            );
        }
        return triggerKey;
    }
    //#endregion
}
