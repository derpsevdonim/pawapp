
import { Injectable } from '@angular/core';
import { Preferences } from '@capacitor/preferences';

@Injectable({ providedIn: 'root' })
export class StorageService{
    //#region __Any
    async getAny( key: string ): Promise<any> {
        const ret = await Preferences.get({ key });
        return ret.value ? JSON.parse(ret.value) : null;
    }

    async setAny( key: string, value: any ): Promise<any> {
        await Preferences.set({ key, value: this.#safeStringify(value) });
    }
    
    async delAny( key: string ): Promise<void> {
        await Preferences.remove({ key });
    }

    async clearAll(): Promise<void> { await Preferences.clear(); }
    //#endregion
    //#region __String
    async setAsString( key: string, value: string ){
        await Preferences.set({ key, value });
    }

    async getAsString( key: string ): Promise<string|null> {
        let ret = await Preferences.get({ key });
        return ret ? ret.value : null;
    }
    //#endregion
    //#region __Flags
    async getAsBoolean( key: string ): Promise<boolean> {
        let ret = await Preferences.get({ key });
        return !!ret.value;
    }

    async setAsBoolean( key: string, value:boolean ){
        value
        ? Preferences.set({ key, value: 'T' })
        : Preferences.remove({ key });
    }
    //#endregion
    //#region __Objects
    async setAsObject( key: string, value: any ){
        return await Preferences.set({ key, value: this.#safeStringify(value) });
    }

    async getAsObject(key: string): Promise<any> {
        const ret = await Preferences.get({ key });
        if( !(ret?.value) ) return null;

        let object;
        try{ object = JSON.parse(ret.value); }
        catch( e ){
            console.warn(`[!] Wrong stored ${key}-object:`, ret.value, e);
            object = null;
        }

        return object;
    }
    //#endregion
    //#region __
    #safeStringify( obj:any ): string {
        return JSON.stringify(obj, (k, v)=> k[0]==='$' ? undefined : v );
    }
    //#endregion
}
