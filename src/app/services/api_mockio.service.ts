import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable, OperatorFunction, catchError, of } from "rxjs";

import { AppService } from "src/app/services/app.service";

import { FetchingOptions, IAnyApiService, RequestParams_common } from "src/app/types/app.types";
import { CatType } from "src/app/types/ent.types";
import { AmService } from "./am.service";

@Injectable({ providedIn: 'root' })
export class ApiMockIoService implements IAnyApiService{

    constructor(
        private am: AmService,
        private app: AppService,
        private hcli: HttpClient,
    ){}

    //#region __I
    fetchCats$( options?:FetchingOptions ): Observable<CatType[]>{
        const catsUrl = this.#getCatsEndpoint();
        if( !catsUrl ) return of([]);
            
        return this.hcli
            .get<CatType[]>(
                catsUrl, {
                    headers: this.#makeBasicHeaders(),
                    params: this.#makeRequestParams(options),
                }
            )
            .pipe( this.#getCommonErrorHandler<CatType[]>('fetching_cats', []) );
    }

    fetchCat$( catId: string ): Observable<CatType|null>{
        const catsUrl = this.#getCatsEndpoint();
        if( !catsUrl || !catId ) return of(null);

        const url = `${catsUrl}/${catId}`;
        return this.hcli
            .get<CatType>(
                url, { headers: this.#makeBasicHeaders() }
            )
            .pipe( this.#getCommonErrorHandler<CatType>('fetching_cat') );
    }
    //#endregion

    //#region __IAnyApiService
    getFetchingMethod( key:string ): string | undefined {
        return {
            cats: 'fetchCats$'
        }[key];
    }
    //#endregion

    //#region __
    #getCatsEndpoint(): string | null {
        if( !this.app.url_mockio ) return null;
        return this.app
            .userAuth
            ? `${this.app.url_mockio}/cats/premium-cats`
            : `${this.app.url_mockio}/cats/cats`;
    }

    #makeBasicHeaders(){
        let headers = new HttpHeaders({
            Accept: 'application/json',
            'content-type':'application/json'
        });
        if( this.app.userAuth ) headers = headers.append(
            'Authorization',
            'Basic ' + btoa(`${this.app.userAuth.email}:${this.app.userAuth.pass}`)
        )
        return headers;
    }

    #makeRequestParams( options:FetchingOptions = {} ){
        const params:RequestParams_common = {};
        if( options.searchQuery ) params.search = options.searchQuery;
        if( options.pageSize ){
            params.l = options.pageSize;
            params.p = 1;
        }
        if( options.pageNum ) params.p = options.pageNum;
        return new HttpParams({fromObject: params});
    }
    
    #getCommonErrorHandler<T>( requestCode:string, noDataHolder?:any ): OperatorFunction<T, any>{
        return catchError( error =>{
            if( error instanceof HttpErrorResponse && error.status === 404 && noDataHolder )
                return of(noDataHolder)

            console.error('[!] API error:', error);
            this.am.noteError(
                error instanceof HttpErrorResponse
                ? `Произошла ошибка при "${requestCode}"-обращении к серверу: ${error.message}`
                : `Произошла ошибка при отправке запроса "${requestCode}"`
            );
            return of(null);
        });
    }
    //#endregion
}
