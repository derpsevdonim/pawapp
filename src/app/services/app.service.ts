import { Injectable, NgZone } from "@angular/core";

const DEFAULTS: any = {
    url_mockio: 'https://66101e9b0640280f219c53ec.mockapi.io/api/v1',
};

@Injectable({ providedIn: 'root' })
export class AppService{
    _: any = {};
    #patch: any = {};
    url_mockio!: string;
    userAuth!: {
        email: string,
        pass: string
    };
    test_num = 12;
    test_string = 'string';
    test_true = true;
    test_false = false;

    constructor( private ngz: NgZone ){
        window.$app = this;
        for( let key in DEFAULTS ) this._[key] = (this as any)[key] = DEFAULTS[key];

        this.ngz.onStable.subscribe(()=>{
            for( let key in this.#patch ){
                this._[key] = this.#patch[key];
                delete this.#patch[key];
            }
        });
    }

    patch( key: string, val: any ){
        (this as any)[key] = this.#patch[key] = val;
    }

    reset(){
        for( let key in DEFAULTS )
            this.patch(key, DEFAULTS[key]);
    }
}
