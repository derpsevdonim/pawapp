Paws App - Story
================

- [x] **Stage 1** - Displaying Cat Cards and Detail Page
- [x] **Stage 2** - Fetching Data from API
- [x] **Stage 3** - Authentication
- [x] **Stage 4** - Premium Cats Listing
- [x] **Stage 5** - Search and Pagination

## App structure

- _Page_ **List of adoptable cats**, items:
    * **Cat's card**: [Model](#M)
        - Image
        - Name
        - Age
        - Breed
        - Adoption fee (premium)
        - Adoption status mark
        - _Action_: View "Cat detail page" [➡️](#cat_page)
    * **Login button**
        - _Action_: Go to "Login page" [➡️](#login_page)
    * [+] **Auth state info**
        - _Story_: Authentificated user can check his status and view his email
    * [+] **Logout button**
        - _Action_: Cancel the premium access
    * **Search bar** [API](#S)
        - Text input
        - _Story_: User searchs for cats via a text input by name, description, or other properties
    * **Pagination** [API](#P)
        - Pages
        - _Story_: User explore a large number of cats by pages

<a name="cat_page" id="cat_page"></a> 
- _Page_ **Cat detail page**:
    * Big image
    * Name
    * Age
    * Breed
    * Adoption fee (premium)
    * Adoption status mark
    * Description
    * Contacts

<a name="login_page" id="login_page"></a> 
- _Page_ **Login page**:   
    * e-mail input
    * password input\
    &nbsp;
    * _accept any combination of credentials_
    * [+] _email validation_\
    &nbsp;

## API
###### Base URL
https://66101e9b0640280f219c53ec.mockapi.io/api/v1
###### Endpoints
- GET ...**/cats/cats** - to get cats
- GET ...**/cats/cats/{CAT_ID}** - to get a cat
- GET ...**/cats/premium-cats** - to get premium cats
- GET ...**/cats/premium-cats/{CAT_ID}** - to get one

#### Authentication
- it is a basic authentication
- should persist across page reloads
- authentificated users view only **premium** cats
- not authentificated users view only **ordinary** cats

#### Search       <a name="S" id="S"></a> 
- GET ...**?search=**{QUERY} - to get all items matching {QUERY}-string in any of the fields
- [Docs](https://github.com/mockapi-io/docs/wiki/Code-examples#filtering)

#### Pagination   <a name="P" id="P"></a> 
###### Server pagination  [[!] blocked](https://github.com/mockapi-io/docs/discussions/8)
- GET ...**?page=**{PAGE_NUM}**&limit=**{PAGE_SIZE} - to get {PAGE_SIZE}-count of items for {PAGE_NUM}-page
- GET ...**?p=**{PAGE_NUM}**&l=**{PAGE_SIZE} - an alternative short variant
- [Docs](https://github.com/mockapi-io/docs/wiki/Code-examples#pagination)
###### Client pagination
- [+] Dynamic page size

## Data models    <a name="M" id="M"></a> 
- **cat**:
    * id
    * listed_at -  _rec creaton timestamp_
    * name
    * image
    * age
    * breed
    * description
    * contact_name
    * contact_phone\
    &nbsp; 
    * adoption_fee - _only for premium cats_
    * is_adopted - 🤔\
    &nbsp;

    * A **regular Cat** example:

            id:  "1"
            age:  "4"
            name:  "Bailey"
            breed:  "Savannah"
            image:  "https://loremflickr.com/640/480/cats"
            contact_name:  "Abel Borer"
            contact_phone:  "539-908-9870 x3492"
            description:  "Delectus praesentium aperiam adipisci culpa tenetur facilis voluptatem nemo ut. Commodi quisquam repudiandae voluptatem quasi nam dolorum. At corporis quia magni blanditiis pariatur dolorem. Iure quas officia impedit laborum. Molestiae qui vero."
            listed_at:  "2024-04-06T00:38:02.451Z"

    * A **premium Cat** example:

            adoption_fee: "303.54"
            is_adopted: false

            id: "1"
            age: "8"
            name: "Harper"
            breed: "Havana"
            image: "https://loremflickr.com/640/480/cats"
            contact_name: "Noel Auer"
            contact_phone: "(787) 204-5503"
            description: "Praesentium voluptatem dolorum placeat omnis illum. Error molestias numquam porro similique. Eaque harum voluptatibus explicabo iste. Incidunt quas quae voluptatibus dolorum quod nam aut. Aliquid eaque vitae esse vitae deserunt perspiciatis facere eos."
            listed_at: "2024-04-05T03:10:48.230Z"

## Tech
- [Ionic lib](https://ionicframework.com) (to kickstart the UX)
- [Angular](https://angular.io/docs) (must have)
- [Markdown](https://www.markdownguide.org/basic-syntax/) 🤓
